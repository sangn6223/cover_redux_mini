import logo from "./logo.svg";
import "./App.css";
import Mini from "./Main/Mini";

function App() {
  return (
    <div className="App">
      <Mini />
    </div>
  );
}

export default App;
