import React, { Component } from "react";
import { connect } from "react-redux";

class Mini extends Component {
  render() {
    console.log("props Sang", this.props);
    return (
      <div>
        <div className="my-5">
          <button
            className="btn btn-warning"
            onClick={this.props.handleGiam}
            style={{ padding: "5px 15px" }}
          >
            -
          </button>
          <strong className="mx-3" style={{ fontSize: "20px" }}>
            {this.props.currentnumber}
          </strong>
          <button
            className="btn btn-success"
            onClick={this.props.handleTang}
            style={{ padding: "5px 15px" }}
          >
            +
          </button>
        </div>
      </div>
    );
  }
}
let mapStatetoProps = (state) => {
  return {
    currentnumber: state.soLuong.number,
  };
};
//--------------------
let mapDispatchTopProps = (dispatch) => {
  return {
    handleTang: () => {
      let action = {
        type: "Tang_So_Luong",
      };
      dispatch(action);
    },
    handleGiam: () => {
      let action = {
        type: "Giam_So_Luong",
      };
      dispatch(action);
    },
  };
};

export default connect(mapStatetoProps, mapDispatchTopProps)(Mini);
