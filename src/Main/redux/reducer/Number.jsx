let initialState = { number: 1 };

export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case "Tang_So_Luong": {
      console.log("yes");
      state.number++;
      return { ...state };
    }
    case "Giam_So_Luong": {
      state.number--;
      return { ...state };
    }
    default: {
      return state;
    }
  }
};
